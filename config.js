/** Public website runtime configuration */

module.exports = {
  title: 'site.title',
  description: 'site.description',
  author: 'AMIV an der ETH, Sandro Lutz',
  siteUrl: `https://amiv.ethz.ch`,
  apiUrl: process.env.NEXT_PUBLIC_API_DOMAIN
    ? `https://${process.env.NEXT_PUBLIC_API_DOMAIN}`
    : 'https://api-dev.amiv.ethz.ch',
  OAuthId: process.env.NEXT_PUBLIC_OAUTH_ID || 'Local Tool',
}
