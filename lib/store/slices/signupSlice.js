import generateSlice from './commonSlice'

const EVENTSIGNUPS = 'eventsignups'
export { EVENTSIGNUPS }

// const initialFilterValues = {
//     search: ''
// };

// const baseReducer = generateReducer(EVENTS, initialFilterValues)
// const baseReducer = generateReducer()

// const eventSlice = createSlice({
//     name: EVENTS,
//     initialState: baseReducer.createInitialState(),
//     reducers: {
//         ...baseReducer.reducers,
//     }
// })

// const lists = {
//     sig
// }

const { slice, actions } = generateSlice(EVENTSIGNUPS)

export default slice.reducer

export const { listLoadAllPages, setQuery, patchItem } = actions
