import generateSlice from './commonSlice'

const EVENTS = 'events'
export { EVENTS }

// const initialFilterValues = {
//     search: ''
// };

// const baseReducer = generateReducer(EVENTS, initialFilterValues)
// const baseReducer = generateReducer()

// const eventSlice = createSlice({
//     name: EVENTS,
//     initialState: baseReducer.createInitialState(),
//     reducers: {
//         ...baseReducer.reducers,
//     }
// })

// const lists = {
//   all: {},
// }

const { slice, actions } = generateSlice(EVENTS)

export default slice.reducer

const { loadItem } = actions

export { loadItem }
