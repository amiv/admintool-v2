import React, { useEffect } from 'react'
import { Box, IconButton, TextField } from '@mui/material'
import SearchIcon from '@mui/icons-material/Search'
import ClearIcon from '@mui/icons-material/Clear'
import {
  initList,
  loadPage,
  setPageSize,
  setSearchString,
} from 'lib/store/slices/listSlice'
import { useDispatch, useSelector } from 'react-redux'
import PropTypes from 'prop-types'

const { DataGrid } = require('@mui/x-data-grid')

// eslint-disable-next-line no-return-assign
const muiPreventDefault = (_, e) => (e.defaultMuiPrevented = true)

const disableCellOutline = {
  '& .MuiDataGrid-columnHeader:focus-within, & .MuiDataGrid-cell:focus-within':
    {
      outline: 'none',
    },

  '& .MuiDataGrid-columnHeader:focus, & .MuiDataGrid-cell:focus': {
    outline: 'none',
  },
}

const QuickSearchToolbar = ({ listName }) => {
  const listState = useSelector(state => state.lists[listName])
  const dispatch = useDispatch()

  return (
    <Box
      sx={{
        p: 0.5,
        pb: 0,
      }}
    >
      <TextField
        variant="standard"
        placeholder="Search…"
        value={listState ? listState.searchString : ''}
        onChange={e => dispatch(setSearchString(listName, e.target.value))}
        InputProps={{
          startAdornment: <SearchIcon fontSize="small" />,
          endAdornment: (
            <IconButton
              title="Clear"
              aria-label="Clear"
              size="small"
              //         // style={{ visibility: props.value ? 'visible' : 'hidden' }}
              //         // onClick={props.clearSearch}
            >
              <ClearIcon fontSize="small" />
            </IconButton>
          ),
        }}
        sx={{
          width: {
            xs: 1,
            sm: 'auto',
          },
          m: theme => theme.spacing(1, 0.5, 1.5),
          '& .MuiSvgIcon-root': {
            mr: 0.5,
          },
          '& .MuiInput-underline:before': {
            borderBottom: 1,
            borderColor: 'divider',
          },
        }}
      ></TextField>
    </Box>
  )
}

QuickSearchToolbar.propTypes = {
  listName: PropTypes.string.isRequired,
}

const List = ({
  resource,
  baseQuery,
  listName,
  columns,
  listProps,
  sx,
  enableSearch = false,
}) => {
  const listState = useSelector(
    state =>
      // console.log("selector", listName, state)
      state.lists[listName]
  )

  const resourceState = useSelector(state => state.lists[resource])

  const dispatch = useDispatch()

  useEffect(() => {
    if (listState === undefined) {
      // dispatch(listSlice.actions.initList({resource: "events", listName: "events", baseQuery: {}}))
      dispatch(
        initList({
          resource,
          listName,
          baseQuery,
          searchFields: columns.map(col => col.field),
        })
      )
    } else {
      //   console.log('listInitialized')
      // dispatch load list state
    }
  })

  if (listState === undefined) {
    return <h1>Loading</h1>
  }

  return (
    <DataGrid
      {...listProps}
      columns={columns}
      sx={{ ...disableCellOutline, ...sx }}
      components={enableSearch ? { Toolbar: QuickSearchToolbar } : {}}
      componentsProps={{
        toolbar: {
          listName,
        },
      }}
      autoHeight
      rows={listState.curPageData.map(id => resourceState.items[id])}
      getRowId={it => it._id}
      loading={listState ? listState.loading > 0 : true}
      paginationMode="server"
      rowCount={listState.rowCount}
      pageSize={listState.pageSize}
      rowsPerPageOptions={[5, 10, 25]}
      onPageSizeChange={pageSize =>
        dispatch(setPageSize({ listName, pageSize })).then(() =>
          dispatch(loadPage(listName, listState.curPage, true))
        )
      }
      onPageChange={page => dispatch(loadPage(listName, page, true))}
      onCellClick={muiPreventDefault}
    ></DataGrid>
  )
}

List.propTypes = {
  listName: PropTypes.string.isRequired,
  resource: PropTypes.string.isRequired,
  baseQuery: PropTypes.object,
  columns: PropTypes.array.isRequired,
  listProps: PropTypes.object,
  sx: PropTypes.object,
  enableSearch: PropTypes.bool,
}

export default List
