const isBrowser = () => typeof window !== `undefined`

const randomString = length => {
  const charset =
    '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._~'
  const random = window.crypto.getRandomValues(new Uint8Array(length))
  const result = []
  random.forEach(c => {
    result.push(charset[c % charset.length])
  })
  return result.join('')
}

export { isBrowser, randomString }
