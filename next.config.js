const runtimeConfig = require('./config')

module.exports = {
  publicRuntimeConfig: runtimeConfig,
  reactStrictMode: true,
  experimental: {
    outputStandalone: true,
  },
  images: {
    domains: [runtimeConfig.apiUrl.replace(/(^\w+:|^)\/\//, '')],
  },
  i18n: {
    // More details about the `default` locale can be found at
    // https://github.com/vercel/next.js/discussions/18419#discussioncomment-1561577
    locales: ['default', 'en', 'de'],
    defaultLocale: 'default',
    localeDetection: false,
  },
  eslint: {
    dirs: ['pages', 'locales', 'lib', 'content'],
  },
  async rewrites() {
    return [
      {
        source: '/media/:path([a-zA-Z0-9]*)/(.*)',
        destination: `${runtimeConfig.apiUrl}/media/:path*`, // Proxy to Backend
      },
    ]
  },
}
