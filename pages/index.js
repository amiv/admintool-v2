import React from 'react'
import Layout from 'lib/components/layout'

const IndexPage = () => {
  return <Layout>...</Layout>
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default IndexPage
