import { useDispatch, useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import Button from '@mui/material/Button'

import { loadItem, EVENTS } from 'lib/store/slices/eventsSlice'
import {
  EVENTSIGNUPS,
  listLoadAllPages,
  setQuery,
} from 'lib/store/slices/signupSlice'
import Layout from 'lib/components/layout'
import List from 'lib/components/common/list'
import { patchItem } from 'lib/store/slices/listSlice'

const rowClasses = {
  '& .row-checked-in , .row-checked-in:hover': {
    bgcolor: 'rgba(0, 255, 0, 0.3)',
  },
  '& .row-checked-out , .row-checked-out:hover': {
    bgcolor: 'rgba(255, 0, 0, 0.15)',
  },
}

const EventsListPage = () => {
  const {
    query: { eventId },
  } = useRouter()

  const dispatch = useDispatch()

  const event = useSelector(state => state.events.items[eventId])

  // Load event if not loaded yet.

  useEffect(() => {
    if (!eventId) return

    // Load event if not already loaded.
    if (!event) {
      dispatch(loadItem(EVENTS, { id: eventId }))
      dispatch(
        setQuery(EVENTSIGNUPS, { query: { where: { event: eventId } } })
      ).then(() => {
        dispatch(listLoadAllPages(EVENTSIGNUPS))
      })
    }
  }, [eventId])
  // const events = useSelector(
  //     state => state.events.all
  // )

  const checkInOutButton = ({ id, row: { _etag, checked_in } }) => (
    <Button
      sx={{
        px: 3,
      }}
      variant="outlined"
      color={checked_in ? 'success' : 'error'}
      onClick={() => {
        dispatch(
          patchItem(EVENTSIGNUPS, {
            id,
            etag: _etag,
            data: { checked_in: !checked_in },
          })
        )
      }}
    >
      Check {checked_in ? 'out' : 'in'}
    </Button>
  )

  const columns = [
    // { field: 'user.name', headerName: 'Name'},
    {
      field: 'email',
      headerName: 'Email',
      flex: 1,
      sortable: false,
      filterable: false,
      hideable: false,
    },
    {
      field: '_id',
      headerName: 'Actions',
      renderCell: checkInOutButton,
      minWidth: 120,
    },
  ]

  return (
    <Layout className="Events">
      {!event || event.isPending ? (
        <h1>loading</h1>
      ) : (
        <>
          <h1>{event.data.title_en}</h1>
          {/* <DataGrid
            sx={rowClasses}
            rows={signups}
            columns={columns}
            getRowId={row => row._id}
            disableColumnFilter
            disableColumnMenu
            disableColumnSelector
            disableSelectionOnClick
            isRowSelectable={() => false}
            getRowClassName={({ row: { checked_in } }) =>
              checked_in ? 'row-checked-in' : 'row-checked-out'
            }
          /> */}
          <List
            resource="eventsignups"
            baseQuery={{ where: { event: eventId } }}
            listName="allSignups"
            columns={columns}
            sx={rowClasses}
            listProps={{
              getRowClassName: ({ row: { checked_in } }) =>
                checked_in ? 'row-checked-in' : 'row-checked-out',
            }}
          />
        </>
      )}
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export const getStaticPaths = async () => {
  return { paths: [], fallback: 'blocking' }
}

export default EventsListPage
