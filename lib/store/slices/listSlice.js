/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit'
import Query from 'lib/utils/query'

const initialListState = {
  items: {},
  pages: [],
  curPageData: [],
  curPage: 0,
  pageSize: 10,
  maxPages: 0,
  rowCount: 0,
  baseQuery: null,
  searchString: '',
  searchFields: [],
  loading: 0,
  resource: null,
  error: null,
}

const initialResourceState = {
  items: {},
}

const newPage = {
  itemIds: [],
  lastLoaded: null,
  isLoading: false,
  error: null,
}

const initialState = {
  errors: [],
}

const listSlice = createSlice({
  name: 'lists',
  initialState,
  reducers: {
    setItemPending: (state, { payload: { resource, id } }) => {
      state[resource].items[id].isLoading = 1
    },
    setItemSuccess: (state, { payload: { resource, id, data } }) => {
      state[resource].items[id] = {
        ...state[resource].items[id],
        ...data,
        isLoading: 0,
      }
    },
    setItemError: (state, { payload: { resource, id, err } }) => {
      state[resource].items[id].isLoading = 0
      state.errors.push({ resource, id, err })
    },
    initList(state, action) {
      const { resource, listName, baseQuery, searchFields } = action.payload
      state[listName] = {
        ...initialListState,
        baseQuery,
        searchFields,
        resource,
      }
      state[resource] = { ...initialResourceState }
    },
    setPagePending(state, { payload: { listName, page } }) {
      state[listName].loading += 1
      state[listName].pages[page] = {
        ...newPage,
        ...state[listName].pages[page],
        isLoading: true,
      }
    },
    setPageSuccess(
      state,
      {
        payload: {
          data: { _items: items, _meta: meta },
          resource,
          listName,
          page,
        },
      }
    ) {
      const thisState = state[listName]
      thisState.curPageData = items.map(item => item._id)
      thisState.curPage = page
      thisState.loading -= 1
      thisState.rowCount = meta.total
      state[resource].items = {
        ...state[resource].items,
        ...items.reduce((acc, item) => ({ ...acc, [item._id]: item }), {}),
      }
    },
    setPageError(state, { payload: { resource, listName, page, err } }) {
      state[listName].loading -= 1
      state[listName].error = err
      state[listName].pages[page].loading = 0
      state[listName].pages[page].error = err
      state.errors.push({ resource, listName, page, err })
    },
    setPageSize(state, { payload: { listName, pageSize } }) {
      state[listName].pageSize = pageSize
    },
    setSearchString(state, { payload: { listName, searchString } }) {
      state[listName].searchString = searchString
    },
  },
})

const loadPage =
  // eslint-disable-next-line no-unused-vars
  (listName, page, forceReload) => async (dispatch, getState) => {
    const { resource, pageSize, searchString, searchFields, baseQuery } =
      getState().lists[listName]

    const searchQuery =
      searchString === ''
        ? {}
        : {
            where: {
              $or: searchFields.map(field => ({
                [field]: { $regex: searchString, $options: 'i' },
              })),
            },
          }

    dispatch({
      types: [
        () => listSlice.actions.setPagePending({ resource, listName, page }),
        data =>
          listSlice.actions.setPageSuccess({ resource, listName, page, data }),
        err =>
          listSlice.actions.setPageError({ resource, listName, page, err }),
      ],
      resource,
      method: 'GET',
      query: Query.merge(searchQuery, baseQuery, {
        page: page + 1,
        max_results: pageSize,
      }),
    })
  }

const initList = listConfig => async dispatch => {
  dispatch(listSlice.actions.initList(listConfig)).then(() => {
    dispatch(loadPage(listConfig.listName, 0, true))
  })
}

const setSearchString =
  (listName, searchString) => async (dispatch, getState) => {
    console.log(listName)
    const { curPage } = getState().lists[listName]
    dispatch(listSlice.actions.setSearchString({ listName, searchString }))
    dispatch(loadPage(listName, curPage, true))
  }

// eslint-disable-next-line no-shadow
const patchItem = (resource, { id, etag, data }) => ({
  types: [
    () => listSlice.actions.setItemPending({ resource, id }),
    // eslint-disable-next-line no-shadow
    data => listSlice.actions.setItemSuccess({ resource, id, data }),
    err => listSlice.actions.setItemError({ resource, id, err }),
  ],
  resource,
  itemId: id,
  method: 'PATCH',
  etag,
  data,
  dataType: 'application/json',
})

export default listSlice.reducer

export const { setPageSize } = listSlice.actions

export { initList, loadPage, setSearchString, patchItem }
