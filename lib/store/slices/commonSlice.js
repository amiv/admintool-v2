import Query from 'lib/utils/query'

const { createSlice } = require('@reduxjs/toolkit')

/** Creates the initial state for a list with the given config and query. */

const generateSlice = resource => {
  const slice = createSlice({
    name: resource,
    initialState: {
      items: {},
      ids: [],
      query: {},
      lastPageLoaded: 0,
      totalPages: 0,
      isPending: false,
      error: null,
    },
    reducers: {
      [`set${resource}ListPending`]: state => ({
        ...state,
        isPending: true,
      }),
      [`set${resource}ListSuccess`]: (state, { payload }) => ({
        ...state,
        items: Object.assign(
          {},
          ...payload._items.map(it => ({
            [it._id]: {
              data: it,
              isPending: false,
              error: null,
            },
          }))
        ),
        ids: Object.keys(state.items),
        lastPageLoaded: payload._meta.page,
        totalPages: Math.max(
          1,
          Math.ceil(payload._meta.total / payload._meta.max_results)
        ),
        isPending: false,
        error: false,
      }),
      [`set${resource}ListError`]: (state, { payload }) => ({
        ...state,
        isPending: false,
        error: payload,
      }),
      [`set${resource}ItemPending`]: (state, { payload: id }) => ({
        ...state,
        items: {
          ...state.items,
          [id]: {
            ...state.items[id],
            isPending: true,
            error: false,
          },
        },
      }),
      [`set${resource}ItemSuccess`]: (state, { payload }) => {
        const id = payload._id

        return {
          ...state,
          items: {
            ...state.items,
            [id]: {
              ...state.items[id],
              data: { ...state.items[id].data, ...payload },
              isPending: false,
              error: false,
            },
          },
        }
      },
      [`set${resource}ItemError`]: (state, action) => ({
        ...state,
        items: {
          ...state.items,
          [action.itemId]: {
            ...state.items[action.itemId],
            isPending: false,
            error: action.error,
          },
        },
      }),
      [`set${resource}Query`]: (state, { payload }) => ({
        ...state,
        query: payload,
      }),
    },
  })
  const actions = {
    // eslint-disable-next-line no-shadow
    setQuery: (resource, { query }) =>
      slice.actions[`set${resource}Query`](query),

    // eslint-disable-next-line no-shadow
    listLoadAllPages: resource => async (dispatch, getState) => {
      const { query } = getState()[resource]

      try {
        const response = await dispatch({
          types: [
            slice.actions[`set${resource}ListPending`],
            null,
            err => slice.actions[`set${resource}ListError`](err),
          ],
          resource,
          method: 'GET',
          query: Query.merge(query, { page: 1 }),
        })

        const pages = { 1: response._items }
        // save totalPages as a constant to avoid race condition with pages added during this
        // process
        const totalPages = Math.ceil(
          response._meta.total / response._meta.max_results
        )

        let data

        if (totalPages <= 1) {
          data = response
        } else {
          await Promise.all(
            Array.from(new Array(totalPages - 1), (x, i) => i + 2).map(
              pageNum =>
                dispatch({
                  types: [null, null, null],
                  resource,
                  method: 'GET',
                  query: Query.merge(query, { page: pageNum }),
                }).then(response2 => {
                  pages[pageNum] = response2._items
                })
            )
          )

          data = {
            ...response,
            _items: [].concat(
              ...Object.keys(pages)
                .sort()
                .map(key => pages[key])
            ),
            _meta: {
              page: totalPages,
              max_results: response._meta.max_results,
              total: response._meta.total,
            },
          }
        }

        dispatch(slice.actions[`set${resource}ListSuccess`](data))
      } catch (err) {
        console.log(err)
        dispatch(slice.actions[`set${resource}ListError`](err))
        // throw(err)
      }
    },

    // listLoadAllPages: (resource) => {
    //     async (dispatch, getState) => {

    //         return slice.actions[`set${resource}ListPending`]

    //         const { query } = getState()[resource]

    // return () => slice.actions[`set${resource}ListError`]({})

    // try {
    // const response = await dispatch({
    //     types: [slice.actions[`set${resource}ListPending`], null, null],
    //     resource,
    //     method: 'GET',
    //     query: Query.merge(query, { page: 1 }),
    // })
    //   dispatch({
    //     type: LIST_SUCCESS(resource),
    //     listName,
    //     append: false,
    //     data,
    //   })

    //             return dispatch(slice.actions[`set${resource}ListSuccess`]({}))
    //         } catch (err) {
    //             dispatch(slice.actions[`set${resource}ListError`](err))
    //             throw err
    //         }
    //     }
    // },

    // eslint-disable-next-line no-shadow
    loadItem: (resource, { id }) => ({
      types: [
        () => slice.actions[`set${resource}ItemPending`](id),
        data => slice.actions[`set${resource}ItemSuccess`](data),
        () => slice.actions[`set${resource}ItemError`](id),
      ],
      resource,
      itemId: id,
      method: 'GET',
    }),

    // eslint-disable-next-line no-shadow
    patchItem: (resource, { id, etag, data }) => ({
      types: [
        () => slice.actions[`set${resource}ItemPending`](id),
        // eslint-disable-next-line no-shadow
        data => slice.actions[`set${resource}ItemSuccess`](data),
        () => slice.actions[`set${resource}ItemError`](id),
      ],
      resource,
      itemId: id,
      method: 'PATCH',
      etag,
      data,
      dataType: 'application/json',
    }),
  }
  return { slice, actions }
}

export default generateSlice

/**
 * Generate a new resource reducer
 * @param {string} resource api resource name
 * @param {object} lists    object with properties (name = list name) and their list configuration
 * @param {object} initialFilterValues initial filter values for the lists.
 */

// const generateReducer = (
//     resource,
//     initialFilterValues = { search: '' }
// ) => ({
//     /**
//      * Generate initialState on-demand.
//      * This is required as the state is dependent on the current time.
//      */
//     createInitialState: () => {
//         return {
//             items: {},
//             filterValues: initialFilterValues,
//             all: _createInitialListState({})
//         }
//     },

//     reducers: {
//         [`set${resource}ItemPending`]: (state, action) => {
//             state.items[action.itemId] = {
//                 ...state.items[action.itemId],
//                 isPending: true,
//                 error: false,
//             }
//         },
//         [`set${resource}ItemSuccess`]: (state, action) => {
//             state.items[action.itemId] = {
//                 data: action.data,
//                 isPending: false,
//                 error: false,
//             }
//         },
//         [`set${resource}ItemError`]: (state, action) => {
//             state.items[action.itemId] = {
//                 ...state.items[action.itemId],
//                 isPending: false,
//                 error: action.error,
//             }
//         }

//     },

//     actions: {
//         /**
//          * Load a specific item.
//          * @param {string} resource     api resource name
//          * @param {string} props.itemId item id
//          */
//         loadItem: (resource, { id }) => ({
//             types: [
//                 ({ type: ITEM_PENDING(resource), itemId: id }),
//                 { type: ITEM_SUCCESS(resource), itemId: id },
//                 { type: ITEM_ERROR(resource), itemId: id },
//             ],
//             resource,
//             itemId: id,
//             method: 'GET',
//         })
//     }

// })

// export default generateReducer
