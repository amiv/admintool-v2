import React from 'react'
import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
  static render() {
    const { language } = this.props

    return (
      <Html lang={language}>
        <Head>
          {/* Static header items should be added here, but there is currently an
              unknown issue preventing them to be rendered.
              See issue https://gitlab.ethz.ch/amiv/amiv-website/-/issues/52 */}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
