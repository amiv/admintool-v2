import { configureStore, combineReducers } from '@reduxjs/toolkit'
import thunk from 'redux-thunk'
import { createWrapper } from 'next-redux-wrapper'

import { isBrowser } from 'lib/utils'

import authReducer from './slices/authSlice'
import eventReducer from './slices/eventsSlice'
import signupReducer from './slices/signupSlice'
import listSlice from './slices/listSlice'

import apiMiddleware from './apiMiddleware'

// preloadedState will be passed in by the plugin
export const store = preloadedState => {
  const rootReducer = combineReducers({
    auth: authReducer,
    events: eventReducer,
    eventsignups: signupReducer,
    lists: listSlice,
  })

  const middleware = [apiMiddleware, thunk]

  // In development, use the browser's Redux dev tools extension if installed
  const isDevelopment = process.env.NODE_ENV === 'development'
  const applyDevTools = isDevelopment && isBrowser() && window.devToolsExtension

  return configureStore({
    reducer: rootReducer,
    middleware,
    preloadedState,
    devTools: applyDevTools,
  })
}

export const wrapper = createWrapper(store)
