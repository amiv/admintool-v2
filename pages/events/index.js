import React from 'react'
import List from 'lib/components/common/list'
import Layout from 'lib/components/layout'
import Link from 'next/link'
import { Button } from '@mui/material'

const checkInOutButton = ({ id }) => (
  <Link passHref href={`/events/${encodeURIComponent(id)}/checkAttendance`}>
    <Button sx={{ px: 3 }} variant="outlined">
      Check-in
    </Button>
  </Link>
  // <Button
  //   sx={{
  //     px: 3,
  //   }}
  //   variant="outlined"
  //   onClick={() => {
  //     dispatch(
  //       patchItem(EVENTSIGNUPS, {
  //         id,
  //         etag: _etag,
  //         data: { checked_in: !checked_in },
  //       })
  //     )
  //   }}
  // >
  //   Check {checked_in ? 'out' : 'in'}
  // </Button>
)

const columns = [
  {
    field: '_id',
    headerName: 'Actions',
    renderCell: checkInOutButton,
    minWidth: 120,
    sortable: false,
    hideable: false,
    filterable: false,
  },
  {
    field: 'title_de',
    headerName: 'title_de',
    flex: 1,
    sortable: false,
    filterable: false,
    hideable: false,
  },
  {
    field: 'title_en',
    headerName: 'title_en',
    flex: 1,
    sortable: false,
    filterable: false,
    hideable: false,
  },
]

const EventsPage = () => {
  return (
    <Layout className="Events">
      <List
        resource="events"
        baseQuery={{}}
        listName="allEvents"
        columns={columns}
        enableSearch
        // listProps={{ columns }}
      />
    </Layout>
  )
}

// Ensures that the page is rerendered on startup of the
// server. Details in the script "docker-entrypoint.sh".
export const getStaticProps = async () => ({ props: {} })

export default EventsPage
