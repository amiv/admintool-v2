import { createSlice } from '@reduxjs/toolkit'

import { isBrowser, randomString } from 'lib/utils'
import { getPublicConfig } from 'lib/utils/config'

import ResourceState from '../ResourceState'

const AUTH_KEY_STATE = 'auth:state'
const AUTH_KEY_TOKEN = 'auth:token'

const initialState = {
  isLoggedIn: false,
  token: null,
  session: null,
  user: null,
  state: ResourceState.Unknown,
  error: null,
  isLocalStorageLoaded: false,
}

const auth = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setLoginPending: state => ({
      ...state,
      state: ResourceState.Loading,
      error: null,
    }),
    setLoginSuccess: (state, { payload }) => {
      localStorage.setItem(AUTH_KEY_TOKEN, payload.token)

      return {
        ...state,
        isLoggedIn: true,
        token: payload.token,
        session: {
          ...state.session,
          ...payload,
          user: payload.user._id,
        },
        state: ResourceState.Loaded,
        isLocalStorageLoaded: true,
        error: null,
      }
    },
    setLoginError: (state, { payload }) => {
      if (isBrowser()) {
        localStorage.removeItem(AUTH_KEY_TOKEN)
      }

      return {
        ...state,
        state: ResourceState.Error,
        error: payload.error,
      }
    },
    setAuthLocalstorageLoaded: state => ({
      ...state,
      isLocalStorageLoaded: true,
    }),
  },
})

export default auth.reducer

const {
  setLoginPending,
  setLoginSuccess,
  setLoginError,
  setAuthLocalstorageLoaded,
} = auth.actions

/**
 * Initiate logout procedure
 */
const authLogout = () => async () => {
  // const { auth: { session } = {} } = getState()
  // if (!session) {
  //   await dispatch({ type: AUTH_LOGOUT_SUCCESS })
  // } else {
  //   await dispatch({
  //     types: [AUTH_LOGOUT_PENDING, AUTH_LOGOUT_SUCCESS, AUTH_LOGOUT_ERROR],
  //     resource: 'sessions',
  //     itemId: session._id,
  //     etag: session._etag,
  //     method: 'DELETE',
  //   })
  // }
}

/**
 * Start OAuth login procedure and redirect to AMIV API OAuth2 page
 */
const authLoginStart = requestedPath => () => {
  const state = randomString(32)
  localStorage.setItem(AUTH_KEY_STATE, state)
  const { OAuthId, apiUrl } = getPublicConfig()

  const redirectPath = requestedPath || window.location.pathname

  const query = new URLSearchParams([
    ['response_type', 'token'],
    ['client_id', OAuthId],
    ['redirect_uri', window.location.origin + redirectPath],
    ['state', state],
  ])

  // Redirect to AMIV API OAuth2 page
  window.location.href = `${apiUrl}/oauth?${query.toString()}`
}

/**
 * Parse search parameters and try to log in with found token
 */
const authLoginBySearchParameters = searchParams => async dispatch => {
  const clearUrlParameters = () =>
    window.history.replaceState({}, document.title, window.location.pathname)

  if (
    searchParams.get('state') &&
    searchParams.get('access_token') &&
    searchParams.get('state') === localStorage.getItem(AUTH_KEY_STATE)
  ) {
    const token = searchParams.get('access_token')

    clearUrlParameters()

    await dispatch({
      types: [setLoginPending, setLoginSuccess, setLoginError],
      resource: 'sessions',
      itemId: token,
      method: 'GET',
      token,
      query: { embedded: { user: 1 } },
    })
  } else {
    clearUrlParameters()
  }
}

/**
 * Read token from localStorage and try to log in if token found
 */
const authLoginByLocalStorageToken = () => async (dispatch, getState) => {
  const { auth: { isLocalStorageLoaded } = {} } = getState()

  if (isLocalStorageLoaded || typeof window === 'undefined') return

  dispatch(setAuthLocalstorageLoaded())

  try {
    const token = localStorage.getItem(AUTH_KEY_TOKEN)

    if (token) {
      await dispatch({
        types: [setLoginPending, setLoginSuccess, setLoginError],
        resource: 'sessions',
        itemId: token,
        method: 'GET',
        token,
        query: { embedded: { user: 1 } },
      })
    }
  } catch (err) {
    // error while reading localStorage. Key might not exist yet.
  }
}

export {
  authLogout,
  authLoginStart,
  authLoginBySearchParameters,
  authLoginByLocalStorageToken,
}
