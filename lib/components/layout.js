import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'

import {
  authLoginByLocalStorageToken,
  authLoginBySearchParameters,
  authLoginStart,
  authLogout,
} from 'lib/store/slices/authSlice'

import {
  AppBar,
  Box,
  IconButton,
  Toolbar,
  Typography,
  Button,
  Drawer,
  useMediaQuery,
  useTheme,
  List,
  ListItem,
} from '@mui/material'

import MenuIcon from '@mui/icons-material/Menu'
import Link from 'next/link'

import { wrapper } from 'lib/store/createStore'

const drawerWidth = 240

const Layout = ({
  // className,
  // seoProps,
  // authenticatedOnly,
  // authenticatedReason,
  children,
}) => {
  // const theme = useMantineTheme()
  const [opened, setOpened] = useState(false)

  const dispatch = useDispatch()
  const { isLocalStorageLoaded, isLoggedIn } = useSelector(state => state.auth)

  // [auth] Load token from localStorage if available
  useEffect(() => {
    if (!isLocalStorageLoaded) {
      dispatch(authLoginByLocalStorageToken())
    }
  })

  // [auth] Try to log in with token from search params if available
  useEffect(() => {
    const searchString = window.location.search
    if (searchString) {
      dispatch(authLoginBySearchParameters(new URLSearchParams(searchString)))
    }
  })

  const drawerContent = (
    <>
      <Toolbar />
      <Box sx={{ overflow: 'auto' }}>
        <List>
          {[['Events', '/events']].map(([name, path]) => (
            <Link passHref href={path} key={name}>
              <ListItem>
                {name}
                {/* <List.Link to={path}>{name}</List.Link> */}
              </ListItem>
            </Link>
          ))}
        </List>
      </Box>
    </>
  )

  const theme = useTheme()
  const permanentDrawer = useMediaQuery(theme.breakpoints.up('sm'))
  const container =
    // eslint-disable-next-line valid-typeof
    typeof window !== undefined ? () => window.document.body : undefined

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar
        position="fixed"
        sx={{
          // eslint-disable-next-line no-shadow
          zIndex: theme => theme.zIndex.drawer + 1,
        }}
      >
        <Toolbar>
          {!permanentDrawer && (
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ mr: 2 }}
              onClick={() => setOpened(!opened)}
            >
              <MenuIcon />
            </IconButton>
          )}
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            AMIV Admintools v2
          </Typography>
          <Button
            color="inherit"
            onClick={() =>
              isLoggedIn ? dispatch(authLogout()) : dispatch(authLoginStart())
            }
          >
            {isLoggedIn ? 'Login' : 'Logout'}
          </Button>
        </Toolbar>
      </AppBar>
      {/* <Box
      component="nav"
      sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
      aria-label="mailbox folders"
    > */}
      {permanentDrawer && (
        <Drawer
          variant="permanent"
          sx={{
            '& .MuiDrawer-paper': {
              boxSizing: 'border-box',
              width: drawerWidth,
            },
          }}
          open
        >
          {drawerContent}
        </Drawer>
      )}
      {!permanentDrawer && (
        <Drawer
          container={container}
          variant="temporary"
          open={opened}
          // onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            '& .MuiDrawer-paper': {
              boxSizing: 'border-box',
              width: drawerWidth,
            },
          }}
        >
          {drawerContent}
        </Drawer>
      )}
      {/* </Box> */}
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          height: '100vh',
          overflow: 'auto',
          ml: { sm: `${drawerWidth}px` },
          p: 3,
        }}
      >
        <Toolbar />
        {children}
      </Box>
    </Box>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  // className: PropTypes.string,
  // seoProps: PropTypes.object.isRequired,
  // authenticatedOnly: PropTypes.bool,
  // authenticatedReason: PropTypes.node,
}

// Layout.defaultProps = {
//   authenticatedOnly: false,
// }

export default wrapper.withRedux(Layout)
