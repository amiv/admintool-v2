import React from 'react'
import PropTypes from 'prop-types'

import { wrapper } from 'lib/store/createStore'

import { ThemeProvider } from 'lib/context/themeContext'
import { Box, CssBaseline } from '@mui/material'

const MyApp = ({ Component, pageProps }) => {
  return (
    <ThemeProvider>
      <CssBaseline />
      <Box sx={{ display: 'flex' }}>
        <Component {...pageProps} />
      </Box>
    </ThemeProvider>
  )
}

MyApp.propTypes = {
  Component: PropTypes.node.isRequired,
  pageProps: PropTypes.object.isRequired,
}

export default wrapper.withRedux(MyApp)
