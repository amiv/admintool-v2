# This Dockerfile uses a multi-stage build. Only the first stage requires
# all dependencies, the final image will contain only the output files

# --------------------------------------------------------------------------
# First stage: Install dependencies
FROM node:18-alpine AS deps

WORKDIR /app

COPY ./package.json .
COPY ./yarn.lock .
COPY ./.yarnrc.yml .
COPY ./.yarn ./.yarn

RUN yarn install --immutable

# --------------------------------------------------------------------------
# First stage: Build project
FROM node:18-alpine AS builder

ENV NEXT_TELEMETRY_DISABLED 1

WORKDIR /app

COPY --from=deps /app/node_modules ./node_modules
COPY --from=deps /app/.yarn ./.yarn

COPY . .

RUN yarn build

# --------------------------------------------------------------------------
# Third stage: Prune dependencies
FROM node:18-alpine AS prune_deps

WORKDIR /app

COPY --from=deps /app/package.json .
COPY --from=deps /app/yarn.lock .
COPY --from=deps /app/.yarnrc.yml .
COPY --from=deps /app/.yarn ./.yarn

RUN yarn workspaces focus --all --production

# --------------------------------------------------------------------------
# Fourth stage: Image for serving the application in production.
FROM node:18-alpine as release

ENV PORT 3000
ENV NODE_ENV production

## Install curl which are needed by the start-up script
RUN apk add --no-cache curl bash

WORKDIR /app
COPY --from=deps /app/package.json .
COPY --from=deps /app/yarn.lock .
COPY --from=prune_deps /app/node_modules ./node_modules
COPY --from=deps /app/.yarn/releases ./.yarn/releases
COPY --chown=node:node --from=builder /app/.next ./.next
COPY next.config.js .
COPY config.js .

# Add start-up script
COPY docker-entrypoint.sh /

USER node

EXPOSE 3000
ENV PORT 3000

# Default run command: starts the Next.js server
# and triggers re-rendering of static pages.
CMD [ "/bin/bash", "/docker-entrypoint.sh" ]
